package root.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by zelenin on 06.01.17.
 */
@Entity
public class Profile {
    private String username;
    private String name;
    private String surname;
    private String email;
    private String avatar;
    private long quota;
    private long engaged;
    private String password;
    private String role;
    private boolean enabled;
    private Long link;

    @Id
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "avatar")
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Basic
    @Column(name = "quota")
    public long getQuota() {
        return quota;
    }

    public void setQuota(long quota) {
        this.quota = quota;
    }

    @Basic
    @Column(name = "engaged")
    public long getEngaged() {
        return engaged;
    }

    public void setEngaged(long engaged) {
        this.engaged = engaged;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Basic
    @Column(name = "enabled")
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "link")
    public Long getLink() {
        return link;
    }

    public void setLink(Long link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Profile profile = (Profile) o;

        if (quota != profile.quota) return false;
        if (engaged != profile.engaged) return false;
        if (enabled != profile.enabled) return false;
        if (username != null ? !username.equals(profile.username) : profile.username != null) return false;
        if (name != null ? !name.equals(profile.name) : profile.name != null) return false;
        if (surname != null ? !surname.equals(profile.surname) : profile.surname != null) return false;
        if (email != null ? !email.equals(profile.email) : profile.email != null) return false;
        if (avatar != null ? !avatar.equals(profile.avatar) : profile.avatar != null) return false;
        if (password != null ? !password.equals(profile.password) : profile.password != null) return false;
        if (role != null ? !role.equals(profile.role) : profile.role != null) return false;
        if (link != null ? !link.equals(profile.link) : profile.link != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (avatar != null ? avatar.hashCode() : 0);
        result = 31 * result + (int) (quota ^ (quota >>> 32));
        result = 31 * result + (int) (engaged ^ (engaged >>> 32));
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        return result;
    }
}
