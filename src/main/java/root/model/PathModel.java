package root.model;


import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by zelenin on 06.01.17.
 */
@Entity
@Table(name = "pathmodel")
public class PathModel {
    private String name;
    private long size;
    private boolean isdir;
    private String type;
    private String owner;
    private String description;
    private String tags;
    private Timestamp createtime;
    private Timestamp lastmodtime;
    private boolean shared;
    private Long parent;
    private long inode;

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "size")
    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Basic
    @Column(name = "isdir")
    public boolean isIsdir() {
        return isdir;
    }

    public void setIsdir(boolean isdir) {
        this.isdir = isdir;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "owner")
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "tags")
    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    @Basic
    @Column(name = "createtime")
    public Timestamp getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Timestamp createtime) {
        this.createtime = createtime;
    }

    @Basic
    @Column(name = "lastmodtime")
    public Timestamp getLastmodtime() {
        return lastmodtime;
    }

    public void setLastmodtime(Timestamp lastmodtime) {
        this.lastmodtime = lastmodtime;
    }

    @Basic
    @Column(name = "shared")
    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    @Basic
    @Column(name = "parent")
    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    @Id
    @Column(name = "inode")
    public long getInode() {
        return inode;
    }

    public void setInode(long inode) {
        this.inode = inode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PathModel pathModel = (PathModel) o;

        if (size != pathModel.size) return false;
        if (isdir != pathModel.isdir) return false;
        if (shared != pathModel.shared) return false;
        if (inode != pathModel.inode) return false;
        if (name != null ? !name.equals(pathModel.name) : pathModel.name != null) return false;
        if (type != null ? !type.equals(pathModel.type) : pathModel.type != null) return false;
        if (owner != null ? !owner.equals(pathModel.owner) : pathModel.owner != null) return false;
        if (description != null ? !description.equals(pathModel.description) : pathModel.description != null) return false;
        if (tags != null ? !tags.equals(pathModel.tags) : pathModel.tags != null) return false;
        if (createtime != null ? !createtime.equals(pathModel.createtime) : pathModel.createtime != null) return false;
        if (lastmodtime != null ? !lastmodtime.equals(pathModel.lastmodtime) : pathModel.lastmodtime != null) return false;
        if (parent != null ? !parent.equals(pathModel.parent) : pathModel.parent != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (int) (size ^ (size >>> 32));
        result = 31 * result + (isdir ? 1 : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (createtime != null ? createtime.hashCode() : 0);
        result = 31 * result + (lastmodtime != null ? lastmodtime.hashCode() : 0);
        result = 31 * result + (shared ? 1 : 0);
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        result = 31 * result + (int) (inode ^ (inode >>> 32));
        return result;
    }
}
