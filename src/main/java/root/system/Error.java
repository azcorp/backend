package root.system;

/**
 * Created by zelenin on 06.12.16.
 */
public class Error {
    public Error(int code,Object message){
        this.code=code;
        this.message=message;
    }
    private int code;
    private Object message;
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public Object getMessage() {
        return message;
    }
    public void setMessage(Object message) {
        this.message = message;
    }

}
