package root.system;


/**
 * Created by zelenin on 23.11.16.
 */
public class WsMessage{
    private String id;
    private String method;
    private Object params;
    private Error error;
    private Object result;
    private String jsonrpc="2.0";

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getMethod() {
        return method;
    }
    public void setMethod(String method) {
        this.method = method;
    }
    public Object getParams() {
        return params;
    }
    public void setParams(Object params) {
        this.params = params;
    }
    public Error getError() {
        return error;
    }
    public void setError(Error error) {
        this.error = error;
    }
    public Object getResult() {
        return result;
    }
    public void setResult(Object result) {
        this.result = result;
    }
}
