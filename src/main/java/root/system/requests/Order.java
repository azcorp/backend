package root.system.requests;

import java.util.List;

/**
 * Created by zelenin on 06.12.16.
 */
public class Order {
    private List<Long> sources;
    private List<Long> targets;
    private Boolean overwrite;
    private String text;
    private String action;
    public List<Long> getSources() {
        return sources;
    }
    public void setSources(List<Long> sources) {
        this.sources = sources;
    }
    public List<Long> getTargets() {
        return targets;
    }
    public void setTargets(List<Long> targets) {
        this.targets = targets;
    }
    public Boolean getOverwrite() {
        return overwrite;
    }
    public void setOverwrite(Boolean overwrite) {
        this.overwrite = overwrite;
    }
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
}
