package root.system.requests;


import java.util.List;
import java.util.Map;

/**
 * Created by zelenin on 28.11.16.
 */
public class Query {
    private String type;
    private int limit;
    private int offset;
    // keys "cr"  "val"
    private List<Map<String,String>> likes;
    private List<Map<String,String>> equals;
    // keys "direct" values "ASC" "DESC"
    private List<Map<String,String>> sort;
    private boolean ignoreCase;
    private List<?> save;

    public List<Map<String, String>> getSort() {
        return sort;
    }
    public void setSort(List<Map<String, String>> sort) {
        this.sort = sort;
    }
    public List<Map<String, String>> getLikes() {
        return likes;
    }
    public void setLikes(List<Map<String, String>> likes) {
        this.likes = likes;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public int getLimit() {
        return limit;
    }
    public void setLimit(int limit) {
        this.limit = limit;
    }
    public int getOffset() {
        return offset;
    }
    public void setOffset(int offset) {
        this.offset = offset;
    }
    public boolean isIgnoreCase() {
        return ignoreCase;
    }
    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }
    public List<Map<String, String>> getEquals() {
        return equals;
    }
    public void setEquals(List<Map<String, String>> equals) {
        this.equals = equals;
    }
    public List<?> getSave() {
        return save;
    }
    public void setSave(List<?> save) {
        this.save = save;
    }

}
