package root.repository;

import org.apache.tika.Tika;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.jboss.logging.Logger;
import org.postgresql.PGConnection;
import org.postgresql.copy.CopyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.stereotype.Service;
import root.model.PathModel;
import root.model.Profile;
import root.service.FilesSystemService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by zelenin on 07.12.16.
 */

@Service
public class PathServiceJPA implements PathService{
    private final PathRepository repository;
    private final FilesSystemService FSS;
    private final ProfileRepository PR;
    private static final Logger log = Logger.getLogger(PathServiceJPA.class);
    private final Tika tika = new Tika();
    @Resource(name = "Vault")
    private String vault;

    @PersistenceContext
    private EntityManager EM;


    @Autowired
    public PathServiceJPA(PathRepository repository, FilesSystemService FSS, ProfileRepository PR) {
        this.repository = repository;
        this.FSS = FSS;
        this.PR = PR;
    }

    @PostConstruct
    public void init() {
        log.info("Create "+ PathServiceJPA.class);
    }

    @Override
    public List<PathModel> findMany(root.system.requests.Query query) {
        CriteriaBuilder cb = EM.getCriteriaBuilder();
        CriteriaQuery<PathModel> cq = cb.createQuery(PathModel.class);
        List<Predicate> predicates = new ArrayList<>();
        Root<PathModel>  root = cq.from(PathModel.class);
        if(query.getLikes()!=null){
            if(query.isIgnoreCase()){
                query.getLikes().forEach(map-> predicates.add(cb.like(cb.lower(root.get(map.get("cr"))),map.get("val").toLowerCase())));
            }
            else {
                query.getLikes().forEach(map-> predicates.add(cb.like(root.get(map.get("cr")),map.get("val"))));
            }
        }
        if(query.getEquals()!=null){
            query.getEquals().forEach(stringLongMap -> {
                predicates.add(
                        cb.equal(
                                root.get(stringLongMap.get("cr")),
                                Long.parseLong(stringLongMap.get("val"))
                        )
                );
            });
        }
        Sort.Order[] orders=null;
        if(query.getSort()!=null){
            orders=new Sort.Order[query.getSort().size()];
            for(int i=0;i<query.getSort().size();i++){
                orders[i]=new Sort.Order(Sort.Direction.fromString(query.getSort().get(i).get("direct")),query.getSort().get(i).get("val"));
            }
        }
        cq.select(root).where(predicates.toArray(new Predicate[]{}));

        if(orders!=null)
        cq.orderBy(QueryUtils.toOrders(new Sort(orders), root, cb));

        TypedQuery<PathModel> typedQuery = EM.createQuery(cq);
        typedQuery.setFirstResult(query.getOffset());
        typedQuery.setMaxResults(query.getLimit());
        return typedQuery.getResultList();
    }

    @Override
    public PathModel findOne(Long inode) {
        return repository.findOne(inode);
    }

    @Override
    public PathModel findOne(String cr, String val) {
        return repository.findOne((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get(cr),val));
    }

    @Override
    public List<PathModel> save(List<PathModel> list) {
        return repository.saveInBatch(list);
    }

    @Override
    public PathModel save(PathModel pathModel) {
        return repository.save(pathModel);
    }

    @Override
    public void deleteOne(Long inode) {
        repository.delete(inode);
    }

    @Override
    public List<Long> deleteMany(List<Long> idlist)  {
       return repository.deleteInBatch(idlist);
    }

    @Override
    public void deleteAll() {
        repository.truncateTabel();
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public long countOwnerEngaged(String username) {
        return repository.checkSumForOwner(username);
    }

    @Override
    public boolean isExist(Long inode) {
        return repository.exists(inode);
    }

    @Override
    public void checkVault() {
        Set<Long> delta = new HashSet<>();
        Set<Long> fails = new HashSet<>();
        long start =0;
        long end =0;

        log.info("File System checking start");
        start = System.currentTimeMillis();
        try {
            Files.walk(Paths.get(vault)).forEach(path->{
                if(!FSS.isLinkedWithStreamer(path)){
                    FSS.linkWithStreamer(path);
                }
                delta.add(FSS.getInodeFromPath(path));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        end = System.currentTimeMillis();
        log.info("File System checking finish. Operation time: " + Long.toString(end-start) + " millis");
        System.out.println(repository.count());


        log.info("DB checking start");
        start = System.currentTimeMillis();
        repository.allInodes().parallelStream().forEach(inode->{
            String p = FSS.getPathFromInode(inode);
            if(p!=null) {
                if(Files.exists(Paths.get(p))){
                    delta.remove(inode);
                }
            }
            else {
                fails.add(inode);
            }
        });
        end = System.currentTimeMillis();
        log.info("DB checking finish Operation time: " + Long.toString(end-start) + " millis");



        start =System.currentTimeMillis();
        repository.deleteInBatch(fails.stream().collect(Collectors.toList()));
        FSS.clearStreamer();
        end = System.currentTimeMillis();
        log.info("Deleting fails "+(end-start)+" millis");

        java.nio.file.Path tmp = Paths.get(vault,"TEMP");
        try {
            start =System.currentTimeMillis();
            exportToFile(tmp,delta.parallelStream().map(inode->getPathModelFromFileSystem(Paths.get(FSS.getPathFromInode(inode)))));
            end = System.currentTimeMillis();
            log.info("Write delta to File "+(end-start)+" millis");

            start =System.currentTimeMillis();
            importFromFile(tmp);
            end = System.currentTimeMillis();
            log.info("Export File to DB "+(end-start)+" millis");

            Files.delete(tmp);
        } catch (IOException e) {
            e.printStackTrace();
        }

        start =System.currentTimeMillis();
        List<Profile> recountList = PR.findAll();
        recountList.forEach(profile -> {
            Long engaged = repository.checkSumForOwner(profile.getUsername());
            if(engaged!=null)
            profile.setEngaged(engaged);
        });
        PR.save(recountList);
        end = System.currentTimeMillis();
        log.info("Export File to DB "+(end-start)+" millis");

       /*
        start =System.currentTimeMillis();
        repository.persistInBatch(delta.parallelStream().map(s->getPathModelFromFileSystem(Paths.get(s))).collect(Collectors.toList()));
        end = System.currentTimeMillis();
        log.info("Persist delta to DB "+(end-start)+" millis");
        */

    }

    @Override
    public void exportToFile(Path path, Stream<PathModel> stream) {
        try (PrintWriter pw = new PrintWriter(new BufferedWriter(Files.newBufferedWriter(path, Charset.forName("UTF-8"))))) {
            stream.forEach(model -> {
                pw.printf("\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\" \n",
                        model.getName(),
                        model.getSize(),
                        model.isIsdir(),

                        model.getType(),
                        model.getOwner(),
                        model.getDescription(),

                        model.getTags(),
                        model.getCreatetime(),
                        model.getLastmodtime(),

                        model.isShared(),
                        model.getParent(),
                        model.getInode());
            });
            pw.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void importFromFile(java.nio.file.Path path) {
    EntityManager em =  EM.getEntityManagerFactory().createEntityManager();
        Session session= em.unwrap(Session.class);
        session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                CopyManager cpManager = ((PGConnection)connection).getCopyAPI();
                try{
                    cpManager.copyIn("COPY pathmodel FROM STDIN WITH CSV", Files.newBufferedReader(path));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        em.close();
    }

    public PathModel getPathModelFromFileSystem(java.nio.file.Path fspath){
        try {
            PathModel model = new PathModel();
            BasicFileAttributes attributes =Files.readAttributes(fspath,BasicFileAttributes.class);
            model.setInode(FSS.getInodeFromPath(fspath));
            model.setParent(FSS.getInodeFromPath(fspath.getParent()));
            model.setName(fspath.getFileName().toString());
            model.setIsdir(attributes.isDirectory());
            if(attributes.isDirectory())model.setType("Folder");
            else model.setType(tika.detect(fspath));
            model.setSize(attributes.size());

            model.setCreatetime(new Timestamp(attributes.creationTime().toMillis()));
            model.setLastmodtime(new Timestamp(attributes.lastModifiedTime().toMillis()));
            if(Paths.get(vault).relativize(fspath).getNameCount()>1){
                model.setOwner(fspath.subpath(3,4).getFileName().toString());
            }

            return model;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
