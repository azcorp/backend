package root.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import root.model.Profile;

import java.util.List;

/**
 * Created by zelenin on 30.06.16.
 */
public interface ProfileRepository extends JpaRepository<Profile,String>,JpaSpecificationExecutor<Profile> {
    @Query(value = "select p.link from Profile p where p.enabled=false ")
    List<Long> allUnenabledProfileLinks();
}
