package root.repository;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import root.model.Profile;
import root.system.requests.Query;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zelenin on 30.06.16.
 */

@Transactional
@Service
public class ProfileServiceJPA implements ProfileService {
    private final ProfileRepository PR;
    private static final Logger log = Logger.getLogger(ProfileServiceJPA.class);

    @Autowired
    public ProfileServiceJPA(ProfileRepository PR) {
        this.PR = PR;
    }

    @PersistenceContext
    private EntityManager EM;

    @PostConstruct
    void init(){
        log.info("Create "+ ProfileServiceJPA.class);
    }


    @Override
    public Profile findOne(String username) {
        return PR.findOne(username);
    }

    @Override
    public Profile findOne(String criteria, String value) {
        return PR.findOne((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(criteria),value));
    }

    @Override
    public List<Profile> findMany(Query query) {
        CriteriaBuilder cb = EM.getCriteriaBuilder();
        CriteriaQuery<Profile> cq = cb.createQuery(Profile.class);
        List<Predicate> predicates = new ArrayList<>();
        Root<Profile> root = cq.from(Profile.class);
        if(query.getLikes()!=null){
            if(query.isIgnoreCase()){
                query.getLikes().forEach(map-> predicates.add(cb.like(cb.lower(root.get(map.get("cr"))),map.get("val").toLowerCase())));
            }
            else {
                query.getLikes().forEach(map-> predicates.add(cb.like(root.get(map.get("cr")),map.get("val"))));
            }
        }
        if(query.getEquals()!=null){
            query.getEquals().forEach(stringLongMap -> {
                predicates.add(
                        cb.equal(
                                root.get(stringLongMap.get("cr")),
                                Long.parseLong(stringLongMap.get("val"))
                        )
                );
            });
        }
        Sort.Order[] orders=null;
        if(query.getSort()!=null){
            orders=new Sort.Order[query.getSort().size()];
            for(int i=0;i<query.getSort().size();i++){
                orders[i]=new Sort.Order(Sort.Direction.fromString(query.getSort().get(i).get("direct")),query.getSort().get(i).get("val"));
            }
        }
        cq.select(root).where(predicates.toArray(new Predicate[]{}));

        if(orders!=null)
            cq.orderBy(QueryUtils.toOrders(new Sort(orders), root, cb));

        TypedQuery<Profile> typedQuery = EM.createQuery(cq);
        typedQuery.setFirstResult(query.getOffset());
        typedQuery.setMaxResults(query.getLimit());
        return typedQuery.getResultList();
    }

    @Override
    public Profile save(Profile profile) {
        return PR.save(profile);
    }

    @Override
    public List<Profile> save(List<Profile> profiles) {
        return PR.save(profiles);
    }

    @Override
    public void delete(Profile profile) {
        PR.delete(profile);
    }

    @Override
    public void delete(List<Profile> profiles) {
        PR.delete(profiles);
    }

    @Override
    public long count() {
        return PR.count();
    }

    @Override
    public List<Long> allUnenabledProfiles() {
       return PR.allUnenabledProfileLinks();
    }
}
