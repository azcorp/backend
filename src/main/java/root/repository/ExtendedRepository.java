package root.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by zelenin on 28.12.16.
 */
@NoRepositoryBean
public interface ExtendedRepository<T, ID extends Serializable> extends JpaRepository<T, ID>  {
    List<T> saveInBatch(List<T> list);
    List<T> persistInBatch(List<T> list);
    List<T> mergeInBatch(List<T> list);
    List<ID> getKeysList();
    void truncateTabel();
    List<ID> deleteInBatch(List<ID> idlist);
}
