package root.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import root.model.PathModel;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by zelenin on 12.09.16.
 */
public interface PathRepository extends ExtendedRepository<PathModel,Long>,JpaSpecificationExecutor<PathModel> {
    @Query(value = "select p.inode from PathModel p")
    List<Long> allInodes();
    @Query(value = "SELECT  SUM(p.size)  FROM PathModel p WHERE p.owner=?1")
    Long checkSumForOwner(String username);
}
