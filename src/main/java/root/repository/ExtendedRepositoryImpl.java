package root.repository;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.postgresql.PGConnection;
import org.postgresql.copy.CopyManager;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by zelenin on 28.12.16.
 */
public class ExtendedRepositoryImpl<T, ID extends Serializable>extends SimpleJpaRepository<T, ID> implements ExtendedRepository<T, ID> {
    private final EntityManager em;
    private final JpaEntityInformation<T, ?> entityInformation;

    public ExtendedRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.em = entityManager;
        this.entityInformation =  entityInformation;
    }

    @Transactional
    @Override
    public List<T> saveInBatch(List<T> list) {
        ArrayList<T> result = new ArrayList<>();
        if(list == null) {
            return result;
        } else{
            for(int i =0;i<list.size();i++){
                if(this.entityInformation.isNew(list.get(i))) {
                    this.em.persist(list.get(i));
                    result.add(list.get(i));
                } else {
                    this.em.merge(list.get(i));
                    result.add(list.get(i));
                }
                if(i % 50 ==0){
                    em.flush();
                    em.clear();
                }
            }
            return result;
        }
    }
    @Transactional
    @Override
    public List<T> persistInBatch(List<T> list) {

        ArrayList<T> result = new ArrayList<>();
        if(list == null) {
            return result;
        } else{
            for(int i =0;i<list.size();i++){
                    this.em.persist(list.get(i));
                    result.add(list.get(i));
                if(i % 50 ==0){
                    em.flush();
                    em.clear();
                    }
                }
            }
        return result;
    }
    @Transactional
    @Override
    public List<T> mergeInBatch(List<T> list) {
        ArrayList<T> result = new ArrayList<>();
        if(list == null) {
            return result;
        } else{
            for(int i =0;i<list.size();i++){
                this.em.merge(list.get(i));
                result.add(list.get(i));
                if(i % 50 ==0){
                    em.flush();
                    em.clear();
                }
            }
        }
        return result;
    }

    @Override
    public List<ID> getKeysList() {
        return em.createNativeQuery("select "+entityInformation.getIdAttribute().getName().toLowerCase()+" from "+entityInformation.getEntityName()).getResultList();
    }

    @Transactional
    @Override
    public void truncateTabel() {
        System.out.println("TRUNCATE TABLE"+entityInformation.getEntityName()+" CONTINUE IDENTITY RESTRICT");
        em.createQuery("TRUNCATE TABLE"+entityInformation.getEntityName().toLowerCase()+" CONTINUE IDENTITY RESTRICT").executeUpdate();
    }

    @Transactional
    @Override
    public List<ID> deleteInBatch(List<ID> idlist) {
        if(idlist!=null&&idlist.size()>0){
            String query = "delete from "+entityInformation.getEntityName()+" p where p."+entityInformation.getIdAttribute().getName()+" in (:param)";
            System.out.println(query);
            System.out.println(idlist);
            em.createQuery(query).setParameter("param",idlist).executeUpdate();
        }
        return idlist;
    }




}



