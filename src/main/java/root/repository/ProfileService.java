package root.repository;

import root.model.Profile;
import root.system.requests.Query;

import java.util.List;

/**
 * Created by zelenin on 30.06.16.
 */
public interface ProfileService {
    Profile findOne(String username);
    Profile findOne(String criteria,String value);
    List<Profile> findMany(Query query);
    Profile save(Profile profile);
    List<Profile> save(List<Profile> profiles);
    void delete(Profile profile);
    void delete(List<Profile> profiles);
    long count();
    List<Long> allUnenabledProfiles();

    /*
    Profile getProfileByUsername(String username);
    Profile getProfileBySurname(String surname);
    Profile getProfileByLink(String link);
    Page<Profile> getProfileList(int page, int size, Sort.Direction sort, String params);
    Profile saveProfile(Profile profile);
    void deleteProfile(String username);
    */
}
