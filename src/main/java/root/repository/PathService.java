package root.repository;

import root.model.PathModel;
import root.system.requests.Query;

import java.util.List;
import java.util.stream.Stream;

/**
 * Createdw by zelenin on 12.09.16.
 */

public interface PathService {
    List<PathModel> findMany(Query query);
    PathModel findOne(Long path);
    PathModel findOne(String cr, String val);

    List<PathModel> save(List<PathModel> list);
    PathModel save(PathModel pathModel);

    void deleteOne(Long path);
    List<Long> deleteMany(List<Long> idlist);
    void deleteAll();

    long count();
    long countOwnerEngaged(String username);
    boolean isExist(Long path);
    void checkVault();
    void exportToFile(java.nio.file.Path path, Stream<PathModel> stream);
    void importFromFile(java.nio.file.Path path);
    PathModel getPathModelFromFileSystem(java.nio.file.Path path);
}
