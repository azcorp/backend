package root.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by zelenin on 13.10.16.
 */
public class LinuxUtil {
    private static final Pattern CLEAR_PATTERN = Pattern.compile("[\\s]+");

    public static String getFileHash(String path) throws IOException, InterruptedException {
        BufferedReader brStdout = new BufferedReader(new InputStreamReader(new ProcessBuilder("md5sum","-b",path).start().getInputStream()));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while((line=brStdout.readLine())!=null){sb.append(line);}
        return sb.toString().substring(0,sb.indexOf("*")-1);
    }

    public static List<String> getIoStat() throws IOException {
        List<String> list= new ArrayList<>();
        BufferedReader brStdout=null;
        brStdout = new BufferedReader(
                new InputStreamReader(
                        new ProcessBuilder("iostat","-kxp")
                                .start().getInputStream()));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while((line=brStdout.readLine())!=null){
            String clearline = CLEAR_PATTERN.matcher(line).replaceAll(" ").trim();
            list.add(clearline);
        }
        return list;
    }
}
