package root.util;

/**
 * Created by zelenin on 15.07.16.
 */
import org.apache.commons.codec.binary.Base64;
import org.jboss.logging.Logger;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;

/**
 * Created by zelenin on 15.07.16.
 */

public class LinkUtil {
    private static final Logger log = Logger.getLogger(LinkUtil.class);
    Base64 coder;
    SecretKeyFactory keyFac;
    PBEParameterSpec pbeParamSpec;
    PBEKeySpec pbeKeySpec;
    SecretKey pbeKey;
    Cipher cipher;
    LinkUtil()  {
        init();
    }
    public static String generateLink(){
        int length = 10;
        Random r = new Random();
        char[] symbols = "abcdifghigklmnopqrstuvwxyz1234567890:".toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i =0; i<length;i++){
            sb.append(symbols[r.nextInt(symbols.length)]);
        }
        return sb.toString();
    }

    public void init()  {
        log.info("Create class "+LinkUtil.class);
        try {
            coder = new Base64(true);
            keyFac = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
            pbeParamSpec = new PBEParameterSpec("qsefthuk".getBytes(), 1);
            pbeKeySpec = new PBEKeySpec("the_super_secret_password".toCharArray());
            pbeKey = keyFac.generateSecret(pbeKeySpec);
            cipher = Cipher.getInstance("PBEWithMD5AndDES");
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    String Base64UrlFromString(String text)  {
        String resault="geting link from link failed";
        try {
            cipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);
          resault  = coder.encodeToString(cipher.doFinal(text.getBytes()));
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return resault;
    }
    public  String StringFromBase64Url(String base64)  {
        String result="geting link from link failed";
        try {
            cipher.init(Cipher.DECRYPT_MODE, pbeKey, pbeParamSpec);
            result = new String(cipher.doFinal(coder.decode(base64.getBytes())));
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
      return result;

    }

}
