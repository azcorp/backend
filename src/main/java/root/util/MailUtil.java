package root.util;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
public class MailUtil {

    private static MailSender mailSender = null;
    private static final Logger log = Logger.getLogger(MailUtil.class);


    @Autowired
    public MailUtil(MailSender mailSender) {
        MailUtil.mailSender = mailSender;
    }

    public static void send(String to, String subject, String msg) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(msg);
        log.info(message.getSubject());
        log.info(message.getText());
        mailSender.send(message);
    }
}