package root.service;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by zelenin on 06.01.17.
 */
@Service
public class FilesSystemService {
    private static final Logger log = Logger.getLogger(FilesSystemService.class);
    private static SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy hh:mm");
    private Field field;
    private TreeMap<Long, Node> vtree = new TreeMap<>();
    @Resource(name = "Vault")
    private String vault;
    @Resource(name = "Streamer")
    private String streamer;

    @PostConstruct
    void init() throws IOException, NoSuchFieldException {
        field = Files.readAttributes(Paths.get(vault), BasicFileAttributes.class).fileKey().getClass().getDeclaredField("st_ino");
        field.setAccessible(true);
        addTree(Files.walk(Paths.get(vault)));
    }


    public Node getNode(Path path) {
        return vtree.get(this.getInodeFromPath(path));
    }

    public Node addNode(Path path) {
        try {
            Object key = Files.readAttributes(path, BasicFileAttributes.class).fileKey();
            Object parentkey = Files.readAttributes(path.getParent(), BasicFileAttributes.class).fileKey();
            Node node = new Node();
            node.setName(path.getFileName().toString());
            node.setParent(vtree.get(field.getLong(parentkey)));
            return vtree.put(field.getLong(key), node);
        } catch (IOException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addTree(Stream<Path> stream) throws IOException, NoSuchFieldException {
        stream.forEach(this::addNode);
    }

    public void deleteTree(Stream<Path> stream) {
        stream.forEach(this::deleteNode);
    }

    public void deleteNode(Path path) {
        try {
            vtree.remove(field.getLong(Files.readAttributes(path, BasicFileAttributes.class).fileKey()));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteNode(Long inode) {
        vtree.remove(inode);

    }

    public Long getInodeFromPath(Path path) {
        Long result = null;
        try {
            result = field.getLong(Files.readAttributes(path, BasicFileAttributes.class).fileKey());
        } catch (IllegalAccessException | IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getPathFromInode(Long inode) {
        Node node = vtree.get(inode);
        if (node != null) {
            String fs = FileSystems.getDefault().getSeparator();
            List<String> path = new ArrayList<>();
            path.add(fs + node.getName());
            while (node != null) {
                node = node.getParent();
                if (node != null) {
                    path.add("/" + node.getName());
                }
            }
            path.add(Paths.get(vault).getParent().toString());
            Collections.reverse(path);
            return path.stream().collect(Collectors.joining());
        } else {
            return null;
        }
    }

    public void writeMetadata(Path path, String name, String value) throws IOException {
        UserDefinedFileAttributeView metadata = Files.getFileAttributeView(path, UserDefinedFileAttributeView.class);
        metadata.write(name, Charset.defaultCharset().encode(value));
    }

    public String readMetadata(Path path, String name) throws IOException {
        UserDefinedFileAttributeView metadata = Files.getFileAttributeView(path, UserDefinedFileAttributeView.class);
        ByteBuffer buffer = ByteBuffer.allocate(metadata.size(name));
        metadata.read(name, buffer);
        buffer.flip();
        return Charset.defaultCharset().decode(buffer).toString();
    }

    public Map<String, List<?>> copy(List<Long> sourceNodes, List<Long> targetNodes, boolean overwrite) {
        List<Long> deltaRemove = new ArrayList<>();
        List<Path> deltaAdd = new ArrayList<>();
        targetNodes.forEach(targetNode -> {
            sourceNodes.forEach(sourceNode -> {
                try {
                    Path sp = Paths.get(this.getPathFromInode(sourceNode));
                    Path dp = Paths.get(this.getPathFromInode(targetNode), sp.getFileName().toString());
                    Files.walk(sp).forEach(from -> {
                        try {
                            Path to = Paths.get(dp.toString(), sp.relativize(from).toString());
                            if (Files.exists(to)) {
                                long deleted = getInodeFromPath(to);
                                if (overwrite & !Files.readAttributes(to, BasicFileAttributes.class).isDirectory()) {
                                    try {
                                        // Collision
                                        deltaAdd.add(Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING));
                                        deltaRemove.add(deleted);
                                        deleteNode(deleted);
                                        tearLinkWithStreamer(to);
                                        // Add new node
                                        addNode(to);
                                        linkWithStreamer(to);

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                deltaAdd.add(Files.copy(from, to));
                                // Set parent
                                addNode(to);
                                linkWithStreamer(to);

                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });
        Map<String, List<?>> result = new HashMap<>();
        result.put("add", deltaAdd);
        result.put("remove", deltaRemove);
        return result;
    }

    public Map<String, List<?>> move(List<Long> sourceNodes, List<Long> targetParentNodes, boolean overwrite) throws IOException {
        List<Long> remove = new ArrayList<>();
        List<Path> unmoved = new ArrayList<>();
        List<Path> moved = new ArrayList<>();
        targetParentNodes.forEach(targetNode -> {
            sourceNodes.forEach(sourceNode -> {
                Path sp = Paths.get(this.getPathFromInode(sourceNode));
                Path dp = Paths.get(this.getPathFromInode(targetNode), sp.getFileName().toString());
                try {
                    List<Path> fromList = new ArrayList<>();
                    Files.walk(sp).forEach(fromList::add);
                    fromList.forEach(from -> {
                        try {
                            Path to = Paths.get(dp.toString(), sp.relativize(from).toString());
                            if (Files.exists(from) & Files.exists(to)) {
                                long deleted = getInodeFromPath(to);
                                if (overwrite & !Files.readAttributes(to, BasicFileAttributes.class).isDirectory()) {
                                    try {
                                        // Collision
                                        moved.add(Files.move(from, to, StandardCopyOption.REPLACE_EXISTING));
                                        remove.add(deleted);
                                        // Change parent
                                        getNode(to).setParent(this.getNode(to.getParent()));

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    unmoved.add(from);
                                }
                            } else {
                                if (Files.exists(from)) {
                                    moved.add(Files.move(from, to));
                                    // Change parent
                                    getNode(to).setParent(this.getNode(to.getParent()));
                                }

                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });


                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
        });
        remove.forEach(del -> {
            try {
                tearLinkWithStreamer(del);
                deleteNode(del);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Map<String, List<?>> result = new HashMap<>();
        result.put("moved", moved);
        result.put("remove", remove);
        result.put("unmoved", unmoved);
        return result;
    }

    public Map<Long, Long> delete(List<Long> targetNodes) {
        Map<Long, Long> result = new HashMap<>();
        targetNodes.forEach(targetsNode -> {
            Path sp = Paths.get(this.getPathFromInode(targetsNode));
            try {
                Files.walk(sp).sorted(Comparator.reverseOrder()).forEach(path -> {
                    try {
                        BasicFileAttributes attributes = Files.readAttributes(path, BasicFileAttributes.class);
                        this.tearLinkWithStreamer(path);
                        this.deleteNode(path);
                        result.put(field.getLong(attributes.fileKey()), attributes.size());
                        Files.deleteIfExists(path);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return result;
    }

    public Path mkdir(Long parentNode, String name) throws IOException {
        Path result = Files.createDirectory(Paths.get(this.getPathFromInode(parentNode), name));
        if (Files.exists(result)) this.addNode(result);
        return result;
    }

    public Path mkfile(Long parentNode, String name) throws IOException {
        Path result = Files.createFile(Paths.get(this.getPathFromInode(parentNode), name));
        if (Files.exists(result)) {
            this.linkWithStreamer(result);
            this.addNode(result);
        }
        return result;
    }

    public Path rename(Long targetNode, String name) throws IOException {
        Path target = Paths.get(this.getPathFromInode(targetNode));
        Path result = Files.move(target, target.getParent().resolve(Paths.get(name)));
        this.getNode(result).setName(name);
        return result;
    }

    public Stream<String> read(Long targetNode) throws IOException {
        Path target = Paths.get(this.getPathFromInode(targetNode));
        BasicFileAttributes attributes = Files.readAttributes(target, BasicFileAttributes.class);
        if (attributes.size() < 5000000) {
            return Files.lines(target, Charset.forName("UTF-8"));
        } else {
            return null;
        }
    }

    public Map<Long, Long> write(Long targetNode, String text) throws IOException {
        Map<Long, Long> result = new HashMap<>();
        Path target = Paths.get(this.getPathFromInode(targetNode));
        BasicFileAttributes attributes = Files.readAttributes(target, BasicFileAttributes.class);
        long size = attributes.size();
        target = Files.write(target, text.getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
        attributes = Files.readAttributes(target, BasicFileAttributes.class);
        result.put(size, attributes.size());
        return result;
    }

    public void linkWithStreamer(Path path) {
        try {
            if (!Files.readAttributes(path, BasicFileAttributes.class).isDirectory()) {
                Path target = Paths.get(streamer, Long.toString(this.getInodeFromPath(path)));
                Files.createLink(target, path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean tearLinkWithStreamer(Path path) throws IOException {
        Path target = Paths.get(streamer, Long.toString(this.getInodeFromPath(path)));
        return Files.deleteIfExists(target);
    }

    public boolean tearLinkWithStreamer(Long path) throws IOException {
        Path target = Paths.get(streamer, Long.toString(path));
        return Files.deleteIfExists(target);
    }

    public boolean isLinkedWithStreamer(Path path) {
        return Files.exists(Paths.get(streamer, Long.toString(this.getInodeFromPath(path))));
    }

    public boolean isLinkedWithVault(Path link) {
        if (link != null) {
            String pathString = this.getPathFromInode(Long.parseLong(link.getFileName().toString()));
            if (pathString != null) {
                return Files.exists(Paths.get(pathString));
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    public void clearStreamer() {
        Path streamerPath = Paths.get(streamer);
        try {
            Files.list(streamerPath).forEach(path -> {
                if (!this.isLinkedWithVault(path)) {
                    try {
                        Files.deleteIfExists(path);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class Node implements Serializable {
        private String name;
        private Node parent;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Node getParent() {
            return parent;
        }

        public void setParent(Node parent) {
            this.parent = parent;
        }
    }

}
