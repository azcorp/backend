package root.controller;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import root.model.Profile;
import root.repository.PathService;
import root.repository.ProfileService;
import root.service.FilesSystemService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.*;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by zelenin on 05.07.16.
 */
@Controller
@RequestMapping(value = "/fs")
public class FileSystemController {
    private final PathService DBPSI;
    private final FilesSystemService FSS;
    private final ProfileService PS;
    private String FS = FileSystems.getDefault().getSeparator();
    private static SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy hh:mm");
    private static final Logger log = Logger.getLogger(FileSystemController.class);
    @Resource(name = "Vault") String rootPath;



    @Autowired
    public FileSystemController(PathService DBPSI, FilesSystemService FSS, ProfileService PS) {
        this.DBPSI = DBPSI;
        this.FSS = FSS;
        this.PS = PS;
    }

    @PostConstruct
    void init(){
        log.info("Create "+FileSystemController.class);
    }

    @RequestMapping(value = "/get/{link}")
    public void getFile(HttpServletResponse response,@PathVariable("link") Long link) throws IOException {
        java.nio.file.Path file = Paths.get(FSS.getPathFromInode(link));
        log.info(file.getFileName().toString());
        if (Files.exists(file))
        {
            response.addHeader("Content-Disposition", "attachment; filename="+file.getFileName().toString().replace(' ','_'));
            response.setHeader("Content-Length", String.format("%s", Files.size(file)));
            response.setHeader("Content-Type", Files.probeContentType(file));

            try
            {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
                response.flushBuffer();
            }
            catch (IOException ex) {
                ex.printStackTrace();
                response.flushBuffer();
            }
        }
    }

    @Secured({"ROLE_USER","ROLE_ADMIN"})
    @RequestMapping(value="/upload", method = RequestMethod.POST)
    public @ResponseBody String upload(MultipartHttpServletRequest request,Principal user) throws IOException, InterruptedException {
        log.info("Upload");
        StringBuilder sb = new StringBuilder();
        Iterator<String> itr =  request.getFileNames();
        MultipartFile mpf = null;
        Profile owner = PS.findOne(user.getName());
        long sumsize = 0;
        while (itr.hasNext()){
            sumsize+=request.getFile(itr.next()).getSize();
        }
        if(owner.getQuota()-owner.getEngaged()>sumsize){
            itr =  request.getFileNames();
            while(itr.hasNext()){
                mpf = request.getFile(itr.next());
                Path p = Paths.get(FSS.getPathFromInode(Long.parseLong(request.getParameter("target"))) + FS + mpf.getOriginalFilename());
                if(Files.exists(p)){
                     sb.append(p.getFileName()).append(" already  exist.");
                }else{
                    Files.copy(mpf.getInputStream(),p);
                    FSS.addNode(p);
                    FSS.linkWithStreamer(p);
                    DBPSI.save(DBPSI.getPathModelFromFileSystem(p));
                    sb.append(p.getFileName().toString()).append(", ");
                }
            }
            owner.setEngaged(owner.getEngaged()+sumsize);
            PS.save(owner);
        }else {
            sb.append("Not enough space in your quota for this files");
        }
        return sb.toString();
    }

}


