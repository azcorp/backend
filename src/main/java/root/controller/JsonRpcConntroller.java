package root.controller;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.WebSocketSession;
import root.model.PathModel;
import root.model.Profile;
import root.repository.PathService;
import root.repository.ProfileService;
import root.service.FilesSystemService;
import root.system.requests.Order;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by zelenin on 23.11.16.
 */
@Controller("JsonRpcController")
public class JsonRpcConntroller {
    private static final Logger log = Logger.getLogger(JsonRpcConntroller.class);
    private static PathService DBPSI;
    private static ProfileService PSI;
    private static FilesSystemService FSS;
    private static ObjectMapper mapper = new ObjectMapper();
    @Resource(name = "Vault")
    private  String Vault;
    @Resource(name = "Streamer")
    private  String Streamer;
    @Resource(name = "ServletServer")
    private  String ServletServer;
    private static Map<String,String> settings = new HashMap<>();

    @PostConstruct
    void init(){
        settings.put("Vault", Vault);
        settings.put("Streamer", Streamer);
        settings.put("ServletServer",ServletServer);
    }

    @Autowired
    public JsonRpcConntroller(PathService pathService, ProfileService profileService, FilesSystemService filesSystemService) {
        PSI = profileService;
        DBPSI = pathService;
        FSS = filesSystemService;
    }
    
    public static List<?> find(JsonNode params, WebSocketSession webSocketSession) throws IOException {
        root.system.requests.Query query = mapper.readValue(params, root.system.requests.Query.class);
        switch (query.getType()){
            case "path":
                return DBPSI.findMany(query);
            case "profile":
                return PSI.findMany(query);
            default:
                return null;
        }
    }

    public static List<?> save(JsonNode params, WebSocketSession webSocketSession) throws IOException {

        List saveList = new ArrayList<>();
        switch (params.get("type").getTextValue()){
            case "path":
                params.get("save").forEach(jsonNode -> {
                    try {
                        saveList.add(mapper.readValue(jsonNode,PathModel.class));
                    } catch (IOException e) {e.printStackTrace();}
                });
                return DBPSI.save(saveList);
            case "profile":
                params.get("save").forEach(jsonNode -> {
                    try {
                        saveList.add(mapper.readValue(jsonNode,Profile.class));
                    } catch (IOException e) {e.printStackTrace();}
                });
                return PSI.save(saveList);
            default:
                return null;
        }
    }

    public static Profile who(JsonNode params, WebSocketSession webSocketSession){
       return PSI.findOne(webSocketSession.getPrincipal().getName());
    }

    public static Map<String,String> settings(JsonNode params, WebSocketSession webSocketSession){
        return  settings;
    }

    public static List<?> fsctrl(JsonNode params, WebSocketSession webSocketSession) throws IOException{
        Order order = mapper.readValue(params,Order.class);
        List<PathModel> DBdelta = new ArrayList<>();
        Path path;

        Profile owner=PSI.findOne(webSocketSession.getPrincipal().getName());
        Map<Long,Long> map;
        Map<String,List<?>> stringListMap;
        final long[] engaged={0};
        switch (order.getAction()){
            case "copy":
                stringListMap = FSS.copy(order.getSources(),order.getTargets(),order.getOverwrite());
                stringListMap.get("add").forEach(p->{
                    DBdelta.add(DBPSI.getPathModelFromFileSystem((Path)p));
                });
                DBPSI.save(DBdelta);
                DBPSI.deleteMany(stringListMap.get("remove").stream().map(p->(Long)p).collect(Collectors.toList()));
                owner.setEngaged(DBPSI.countOwnerEngaged(owner.getUsername()));
                PSI.save(owner);
                return DBdelta;
            case "move":
                stringListMap = FSS.move(order.getSources(),order.getTargets(),order.getOverwrite());
                DBPSI.deleteMany(stringListMap.get("remove").stream().map(p->(Long)p).collect(Collectors.toList()));
                DBPSI.deleteMany(stringListMap.get("unmoved").stream().map(p->(Path)p).sorted(Comparator.reverseOrder()).map(p->{
                    long d = FSS.getInodeFromPath(p);
                    try {
                        FSS.tearLinkWithStreamer(p);
                        Files.deleteIfExists(p);
                    }
                    catch (IOException e) {e.printStackTrace();
                    }
                    return d;
                }).collect(Collectors.toList()));
                DBPSI.save(stringListMap.get("moved").stream().map(p->{
                   PathModel model = DBPSI.findOne(FSS.getInodeFromPath((Path)p));
                   model.setParent(FSS.getInodeFromPath(((Path) p).getParent()));
                   return model;
                }).collect(Collectors.toList()));
                owner.setEngaged(DBPSI.countOwnerEngaged(owner.getUsername()));
                PSI.save(owner);
                return DBdelta;
            case "delete":
                map= FSS.delete(order.getTargets());
                engaged[0]=owner.getEngaged();
                List<Long> list = new ArrayList<>();
                map.forEach((aLong, aLong2) -> {
                    list.add(aLong);
                    engaged[0] -= aLong2;
                });
                owner.setEngaged(engaged[0]);
                PSI.save(owner);
                return DBPSI.deleteMany(list);
            case "mkdir":
                path = FSS.mkdir(order.getTargets().get(0),order.getText());
                owner.setEngaged(owner.getEngaged()+4096);
                PSI.save(owner);
                return  Arrays.asList(DBPSI.save(DBPSI.getPathModelFromFileSystem(path)));
            case "mkfile":
                path = FSS.mkfile(order.getTargets().get(0),order.getText());
                return Arrays.asList(DBPSI.save(DBPSI.getPathModelFromFileSystem(path)));
            case "rename":
                path = FSS.rename(order.getTargets().get(0),order.getText());
                return Arrays.asList(DBPSI.save(DBPSI.getPathModelFromFileSystem(path)));
            case "read":
                System.out.println(order.getTargets());
                return FSS.read(order.getTargets().get(0)).collect(Collectors.toList());
            case "write":
                map= FSS.write(order.getTargets().get(0),order.getText());
                final long[] key = {0};
                map.forEach((aLong, aLong2) -> {
                    key[0] =aLong;
                    engaged[0] =aLong2 - aLong ;
                });
                owner.setEngaged(owner.getEngaged()+engaged[0]);
                PSI.save(owner);
                DBdelta.add(DBPSI.findOne(order.getTargets().get(0)));
                DBdelta.get(0).setSize(map.get(key[0]));
                return DBPSI.save(DBdelta);
            default:
                return null;
        }
    }





}
