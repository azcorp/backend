package root.controller;

/**
 * Created by zelenin on 08.06.16.
 */

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import root.model.Profile;
import root.repository.PathService;
import root.repository.ProfileService;
import root.service.FilesSystemService;
import root.util.MailUtil;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
public class Main {

    private static final Logger log = Logger.getLogger(Main.class);
    private String FS = FileSystems.getDefault().getSeparator();
    private final ProfileService PSI;
    private final FilesSystemService FSS;
    private final PathService PS;
    @Resource(name = "Vault")
    private  String vault;
    @Resource(name = "ServletServer")
    private  String ServletServer;


    @Autowired
    public Main(ProfileService PSI, FilesSystemService FSS, PathService PS) {
        this.PSI = PSI;
        this.FSS = FSS;
        this.PS = PS;
    }

    @PostConstruct
    void init(){
        log.info("Create "+Main.class);
    }

    @RequestMapping(value = "/registration",method = RequestMethod.POST)
    public @ResponseBody
    String create(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestParam("name") String name,
            @RequestParam("surname") String surname,
            @RequestParam("email") String email
    ) throws IOException {
        System.out.println("User creation");
        String result="";
        log.info(username);
        log.info(password);
        log.info(name);
        log.info(surname);
        log.info(email);
        Long link = FSS.getInodeFromPath(FSS.mkdir(FSS.getInodeFromPath(Paths.get(vault)),username));
        Profile profile = new Profile();
        profile.setUsername(username);
        profile.setPassword(new BCryptPasswordEncoder().encode(password));
        profile.setRole("ROLE_USER");
        profile.setEnabled(false);
        profile.setName(name);
        profile.setSurname(surname);
        profile.setEmail(email);
        profile.setLink(link);
        profile.setQuota(5000000000L);
        profile.setEngaged(0L);
        PSI.save(profile);
        String message="http://"+ServletServer+"/activate/"+link;
        MailUtil.send(email,"ZTower Activation link",message);
        result = "<h3>Email with activate link send to your email</h3>";
        return result;
    }
    @RequestMapping(value = "/activate/{link}")
    public @ResponseBody String activate(@PathVariable("link")String link) throws IOException {
        Profile profile = PSI.findOne("link",link);
        if(profile != null){
            profile.setEnabled(true);
            PSI.save(profile);
            return "<h5>Activated.</h5>" +
                    "<a href=\"http://ztower.ml/login.html\"></a>";
        } else {
            return "No Profile in this link";
        }
    }

    @RequestMapping(value = "/test")
    public @ResponseBody String test(){
        return "Wildfly is still flying! Test 5";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/adapt")
    public @ResponseBody String checkVault(){
        PS.checkVault();
        FSS.delete(PSI.allUnenabledProfiles());
        return "Adapted";
    }

}