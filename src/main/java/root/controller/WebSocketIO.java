package root.controller;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.*;
import root.system.Error;
import root.system.WsMessage;


import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by zelenin on 23.11.16.
 */
@Controller
public class WebSocketIO implements WebSocketHandler {
    private static final Logger log = Logger.getLogger(WebSocketIO.class);
    private static ObjectMapper mapper = new ObjectMapper();
    private static Map<String,Method> methods = new HashMap<>();
    private final ApplicationContext AC;

    @Autowired
    public WebSocketIO(ApplicationContext AC) {
        this.AC = AC;
    }

    @PostConstruct
    void init() throws NoSuchFieldException, IllegalAccessException, InvocationTargetException {
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        log.info("Create "+WebSocketIO.class);

        Class C = AopUtils.getTargetClass(AC.getBean("JsonRpcController"));
        for(Method method : C.getDeclaredMethods()){
            methods.put(method.getName(),method);
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {


    }

    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
        WsMessage wsm = new WsMessage();
        JsonNode jn=null;
        long start = System.currentTimeMillis();
        try {
            jn = mapper.readTree(webSocketMessage.getPayload().toString());
            log.info(jn);
            wsm.setMethod((jn.get("method").getTextValue()));
            wsm.setId(jn.get("id").getTextValue());
            wsm.setResult(methods.get(wsm.getMethod()).invoke(wsm.getMethod(),jn.get("params"),webSocketSession));
            wsm.setParams(null);
            wsm.setMethod(null);
            webSocketSession.sendMessage(new TextMessage(mapper.writeValueAsString(wsm)));
        }catch (JsonParseException e){
            wsm.setError(new root.system.Error(-32700,e));
            wsm.setId("null");
            webSocketSession.sendMessage(new TextMessage(mapper.writeValueAsString(wsm)));
        }catch (Exception e){
            e.printStackTrace();
            log.error(e);
            wsm.setError(new Error(-32603,e));
            wsm.setId(jn.get("id").getTextValue());
            webSocketSession.sendMessage(new TextMessage(mapper.writeValueAsString(wsm)));
        }
        long end = System.currentTimeMillis();
        log.info(wsm.getId()+" "+Long.toString(end-start));
    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {
        log.info(webSocketSession.getId()+" "+webSocketSession.getRemoteAddress());
        log.error(throwable);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {
        log.info(closeStatus);

    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
}
