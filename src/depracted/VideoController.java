package root.controller;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import root.repository.PathService;
import root.service.FilesSystemService;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by zelenin on 02.08.16.
 */
@Controller
@RequestMapping(value = "/video")
public class VideoController {

    private static final Logger log = Logger.getLogger(VideoController.class);
    private final PathService DBPSI;
    private final FilesSystemService FSS;

    @Autowired
    public VideoController(PathService DBPSI, FilesSystemService FSS) {
        this.DBPSI = DBPSI;
        this.FSS = FSS;
    }

    @PostConstruct
    void init(){
        log.info("Create "+VideoController.class);
    }

    @Secured({"ROLE_USER","ROLE_ADMIN"})
    @RequestMapping(value = "/play/{link}", method = RequestMethod.GET)
    public void getVideoStream(@PathVariable Long link, HttpServletResponse response, HttpServletRequest request) throws IOException {
        String path = FSS.getPathFromInode(link);
        System.out.println(path);
        final Pattern RANGE_PATTERN = Pattern.compile("bytes=(?<start>\\d*)-(?<end>\\d*)");
        final int BUFFER_LENGTH = 1024 * 16;
        final long EXPIRE_TIME = 1000 * 60 * 60 * 24;
        try {
            Path video = Paths.get(path);
            int length = (int) Files.size(video);
            int start = 0;
            int end = length - 1;
            String range = request.getHeader("Range");
            if (range != null) {
                Matcher matcher = RANGE_PATTERN.matcher(range);
                if (matcher.matches()) {
                    String startGroup = matcher.group("start");
                    start = startGroup.isEmpty() ? start : Integer.valueOf(startGroup);
                    start = start < 0 ? 0 : start;
                    String endGroup = matcher.group("end");
                    end = endGroup.isEmpty() ? end : Integer.valueOf(endGroup);
                    end = end > length - 1 ? length - 1 : end;
                }
            }
            int contentLength = end - start + 1;
            response.reset();
            response.setBufferSize(BUFFER_LENGTH);
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            response.setHeader("Content-Disposition", "inline; filename="+video.getFileName().toString().replace(" ", "_"));
            response.setHeader("Accept-Ranges", "bytes");
            response.setDateHeader("Last-Modified", Files.getLastModifiedTime(video).toMillis());
            response.setDateHeader("Expires", System.currentTimeMillis() + EXPIRE_TIME);
            response.setContentType(Files.probeContentType(video));
            response.setHeader("Content-Range", String.format("bytes %s-%s/%s", start, end, length));
            response.setHeader("Content-Length", String.format("%s", contentLength));
            response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
            int bytesRead;
            int bytesLeft = contentLength;
            ByteBuffer buffer = ByteBuffer.allocate(BUFFER_LENGTH);
            try (SeekableByteChannel input = Files.newByteChannel(video, StandardOpenOption.READ); OutputStream output = response.getOutputStream()) {
                input.position(start);
                while ((bytesRead = input.read(buffer)) != -1 && bytesLeft > 0) {
                    buffer.clear();
                    output.write(buffer.array(), 0, bytesLeft < bytesRead ? bytesLeft : bytesRead);
                    bytesLeft -= bytesRead;
                }
                input.close();
                output.close();
            }
            response.flushBuffer();
        } catch (IOException | NumberFormatException e) {
            System.err.println(e);
            response.flushBuffer();
        }
    }

}
