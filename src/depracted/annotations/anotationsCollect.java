
        for(String bean : AC.getBeanNamesForAnnotation(WsServiceType.class)){
            Class C = AopUtils.getTargetClass(AC.getBean(bean));
            Map<String,Method> methods = new HashMap<>();
            String ServiceName="";
            for (Annotation annotation : C.getAnnotationsByType(WsServiceType.class)){
                Class<? extends Annotation> type = annotation.annotationType();
                for (Method method : type.getDeclaredMethods()) {
                    ServiceName = (String) method.invoke(annotation, (Object[])null);
                }
            }
            for(Method method : C.getDeclaredMethods()){
               if(method.isAnnotationPresent(WsAction.class)) methods.put(method.getAnnotation(WsAction.class).name(),method);
            }
            log.info(ServiceName);
            log.info(methods);
            services.put(ServiceName,methods);
        }
