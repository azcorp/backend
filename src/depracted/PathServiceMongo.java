package root.repository.services;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import root.repository.PathRepository;
import root.util.LinkUtil;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zelenin on 12.09.16.
 */


@Service
@Transactional
public class PathServiceMongo implements PathService {

    private final PathRepository PathRepository;
    private final MongoTemplate MT;
    private static SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy hh:mm");
    private static final Logger log = Logger.getLogger(PathServiceMongo.class);
    @Resource(name = "vault") String rootPath;


    @Autowired
    public PathServiceMongo(PathRepository DBPR, MongoTemplate MT) {
        this.PathRepository = DBPR;
        this.MT = MT;
    }

    @PostConstruct
    public void init() {
        log.info("Create "+ PathServiceMongo.class);
        checkVault();
    }

    @Override
    public List<root.model.Path> findMany(root.system.requests.Query query) {
        Criteria cr = new Criteria();
        query.getQuery().forEach(pair->{
            cr.and(pair.get("cr")).regex(pair.get("val"),"i");
        });
        Sort.Order[] orders=new Sort.Order[query.getSort().size()];
        for(int i=0;i<query.getSort().size();i++){
            orders[i]=new Sort.Order(Sort.Direction.fromString(query.getSort().get(i).get("direct")),query.getSort().get(i).get("val"));
        }
        return MT.find(Query.query(cr).with(new Sort(orders)).limit(query.getLimit()), root.model.Path.class);
    }

    @Override
    public root.model.Path findOne(String path) {
        return PathRepository.findOne(path);
    }

    @Override
    public root.model.Path findOne(String cr, String val) {
        return MT.findOne(Query.query(Criteria.where(cr).is(val)),root.model.Path.class);
    }


    @Override
    public List<root.model.Path> getBranch(String path) {
        return MT.find(Query.query(Criteria.where("_id").regex(path)).with(new Sort(Sort.Direction.ASC,"path")), root.model.Path.class);
    }


    @Override
    public List<root.model.Path> save(List<root.model.Path> list) {
        List<root.model.Path> result=new ArrayList<>();
        PathRepository.save(list).forEach(item->result.add(item));
        return result;

    }

    @Override
    public root.model.Path save(root.model.Path path) {
        return PathRepository.save(path);
    }

    @Override
    public void deleteOne(String path) {
            PathRepository.delete(path);
    }


    @Override
    public void deleteBranch(String path) {
        MT.remove(Query.query(Criteria.where("_id").regex(path)), root.model.Path.class);
    }

    @Override
    public void deleteAll() {
        PathRepository.deleteAll();}

    @Override
    public long count() {
        return PathRepository.count();
    }

    @Override
    public boolean isExist(String path) {
        return PathRepository.exists(path);
    }

    @Override
    public void checkVault() {
        log.info("File System checking");
        java.nio.file.Path rp = Paths.get(rootPath);
        List<root.model.Path> fslist = new ArrayList();
        try {
            Files.walkFileTree(rp,new FileVisitor<java.nio.file.Path>() {
                @Override
                public FileVisitResult preVisitDirectory(java.nio.file.Path dir, BasicFileAttributes attrs) throws IOException {
                    root.model.Path dbp = new root.model.Path();
                    dbp.setPath(dir.toAbsolutePath().toString());
                    dbp.setName(dir.getFileName().toString());
                    dbp.setIsdir(true);
                    dbp.setCreatetime(new Time(attrs.creationTime().toMillis()));
                    dbp.setLastmodtime(new Time(attrs.lastModifiedTime().toMillis()));
                    dbp.setLink(LinkUtil.generateLink());
                    dbp.setOwner("System");
                    dbp.setType("Folder");
                    dbp.setSize(attrs.size());
                    dbp.setParent(dir.getParent().toAbsolutePath().toString());
                    dbp.setTags("");
                    fslist.add(dbp);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(java.nio.file.Path file, BasicFileAttributes attrs) throws IOException {
                    root.model.Path dbp = new root.model.Path();
                    dbp.setPath(file.toAbsolutePath().toString());
                    dbp.setName(file.getFileName().toString());
                    dbp.setIsdir(false);
                    dbp.setCreatetime(new Time(attrs.creationTime().toMillis()));
                    dbp.setLastmodtime(new Time(attrs.lastModifiedTime().toMillis()));
                    dbp.setLink(LinkUtil.generateLink());
                    dbp.setOwner("System");
                    dbp.setType(Files.probeContentType(file));
                    dbp.setSize(attrs.size());
                    dbp.setParent(file.getParent().toAbsolutePath().toString());
                    dbp.setTags("");
                    fslist.add(dbp);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(java.nio.file.Path file, IOException exc) throws IOException {
                    return FileVisitResult.SKIP_SIBLINGS;
                }

                @Override
                public FileVisitResult postVisitDirectory(java.nio.file.Path dir, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("File System checked");
        log.info("DB checking");

        List<root.model.Path> dblist = new ArrayList<>();
        PathRepository.findAll().forEach(item->dblist.add(item));
        log.info(dblist);
        if(dblist!=null&&dblist.size()>0){
            fslist.forEach(fspath->{
                if(!dblist.contains(fspath))dblist.add(fspath);
            });
            PathRepository.save(dblist);
        }else PathRepository.save(fslist);


    }


}







