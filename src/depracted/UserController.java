package root.controller;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.security.Principal;
import java.util.List;



/**
 * Created by zelenin on 23.06.16.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {
    private final UserService USI;
    private static final Logger log = Logger.getLogger(UserController.class);
    @Resource(name = "vault") String rootPath;

    @Autowired
    public UserController(UserService USI) {this.USI = USI;}

    @PostConstruct
    void init(){
        log.info("Create "+UserController.class);
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/count")
    public @ResponseBody long getCount(){
        return USI.count();
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/page/{page}/{size}/{vector}/{param}", method = RequestMethod.GET,produces = "application/json")
    public @ResponseBody
    List<User> getList(@PathVariable("page")String page, @PathVariable("size")String size,
                       @PathVariable("vector")String vector, @PathVariable("param") String param){

        return  USI.getUserList(Integer.parseInt(page),
                Integer.parseInt(size),
                Sort.Direction.fromString(vector),param).getContent();
    }
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/{username}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    User getOne(@PathVariable("username")String username){
        return USI.getUser(username);
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value="/", method = RequestMethod.POST,produces = "application/json")
    public @ResponseBody String saveOne(@RequestBody final User userJson) {
        userJson.setPassword(new BCryptPasswordEncoder().encode(userJson.getPassword()));
        String result = "Success";
        try{USI.saveUser(userJson);}catch (Exception e){result = "Fail because" + e;}
        return result;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value="/{username}", method = RequestMethod.DELETE,produces = "application/json")
    public @ResponseBody
    String deleteOne(@RequestBody final User userJson) {

        String result = "Success";
        try{USI.deleteUser(userJson.getUsername());}catch (Exception e){result = "Fail because" + e;}
        return result;
    }
    @Secured({"ROLE_USER","ROLE_ADMIN"})
    @RequestMapping(value = "/changePass", method = RequestMethod.POST,produces = "application/json")
    public @ResponseBody String update(@RequestBody User userJson, Principal user){
        if(user.getName().equalsIgnoreCase(userJson.getUsername())){
            USI.saveUser(userJson);
          return  "Success";
        } else {
          return "You can change only you password";
        }
    }




}
