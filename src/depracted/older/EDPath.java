package out;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zelenin on 05.07.16.
 */


public class EDPath implements Serializable {

    @JsonIgnore
    private SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy hh:mm");

    private String path;
    private String name;
    private byte isDir;
    private long size;
    private String createTime;
    private String lastModTime;
    private String tags;
    private String owner;
    private String link;
    private List<EDPath> content;

    private void init(String path) throws IOException {
        Path pth = Paths.get(path);
        BasicFileAttributes bfa = Files.readAttributes(pth,BasicFileAttributes.class);
        setPath(path);
        setName(pth.getFileName().toString());
        setCreateTime(SDF.format(bfa.creationTime()));
        setLastModTime(SDF.format(bfa.lastModifiedTime()));
        if (bfa.isDirectory()){
            setIsDir((byte)1);
            for (Path p : Files.newDirectoryStream(pth)) {content.add(new EDPath(p.toString()));}
        }else {setIsDir((byte)0);}

    }

    public EDPath(String path) throws IOException {
        setContent(new ArrayList<>());
        init(path);

    }

    private EDPath(String path, Map<String,String> info) throws IOException {
        setContent(new ArrayList<>());
        setName(Paths.get(path).getFileName().toString());
        setPath(path);
    }



    public String getPath() {
        return path;
    }

    private void setPath(String path) throws IOException {this.path=path;}

    public List<EDPath> getContent()  {
        return content;
    }

    public void setContent(List<EDPath> content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SimpleDateFormat getSDF() {
        return SDF;
    }

    public void setSDF(SimpleDateFormat SDF) {
        this.SDF = SDF;
    }

    public byte getIsDir() {
        return isDir;
    }

    public void setIsDir(byte isDir) {
        this.isDir = isDir;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getLastModTime() {
        return lastModTime;
    }

    public void setLastModTime(String lastModTime) {
        this.lastModTime = lastModTime;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
