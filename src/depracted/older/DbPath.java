package out;

import javax.persistence.*;

/**
 * Created by zelenin on 12.09.16.
 */
@Entity
@Table(name = "PathModel")
public class DbPath {
    private String path;
    private String name;
    private byte isDir;
    private long size;
    private String createTime;
    private String lastModTime;
    private String tags;
    private String owner;
    private String link;
    private String parent;
    private String type;

    @Id
    @Column(name = "path")
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "isDir")
    public byte getIsDir() {
        return isDir;
    }

    public void setIsDir(byte isDir) {
        this.isDir = isDir;
    }

    @Basic
    @Column(name = "size")
    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Basic
    @Column(name = "createTime")
    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "lastModTime")
    public String getLastModTime() {
        return lastModTime;
    }

    public void setLastModTime(String lastModTime) {
        this.lastModTime = lastModTime;
    }

    @Basic
    @Column(name = "tags")
    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    @Basic
    @Column(name = "owner")
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Basic
    @Column(name = "link")
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Basic
    @Column(name = "parent")
    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DbPath dbPath = (DbPath) o;

        if (isDir != dbPath.isDir) return false;
        if (size != dbPath.size) return false;
        if (path != null ? !path.equals(dbPath.path) : dbPath.path != null) return false;
        if (name != null ? !name.equals(dbPath.name) : dbPath.name != null) return false;
        if (createTime != null ? !createTime.equals(dbPath.createTime) : dbPath.createTime != null) return false;
        if (lastModTime != null ? !lastModTime.equals(dbPath.lastModTime) : dbPath.lastModTime != null) return false;
        if (tags != null ? !tags.equals(dbPath.tags) : dbPath.tags != null) return false;
        if (owner != null ? !owner.equals(dbPath.owner) : dbPath.owner != null) return false;
        if (link != null ? !link.equals(dbPath.link) : dbPath.link != null) return false;
        if (parent != null ? !parent.equals(dbPath.parent) : dbPath.parent != null) return false;
        if (type != null ? !type.equals(dbPath.type) : dbPath.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = path != null ? path.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) isDir;
        result = 31 * result + (int) (size ^ (size >>> 32));
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (lastModTime != null ? lastModTime.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
