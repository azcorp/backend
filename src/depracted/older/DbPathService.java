package out;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import root.service.FileService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zelenin on 05.09.16.
 */
@Service
@Transactional
@Lazy
public class DbPathService implements DbPathServiceInterface {

    private final DbPathRepository DBPR;
    private static SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy hh:mm");
    @Resource(name = "rootpath")
    String rootPath;

    @PostConstruct
    public void init(){
        checkVault(rootPath);
    }

    @Autowired
    public DbPathService(DbPathRepository DBPR) {
        this.DBPR = DBPR;
    }

    @Override
    public DbPath getByPath(String path) {
        return DBPR.findOne(path);
    }

    @Override
    public DbPath getByLink(String link) {
        return DBPR.findOne((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("link"),link));
    }

    @Override
    public List<DbPath> getByParnet(String path) {
        return DBPR.findAll((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("parent"),path));
    }


    @Override
    public DbPath saveOrUpdateOne(DbPath dbp) {
        return DBPR.save(dbp);
    }

    @Override
    public List<DbPath> saveOrUpdateList(List<DbPath> list) {
        return  DBPR.save(list);
    }

    @Override
    public void deleteOne(String path) {
        DBPR.delete(path);
    }

    @Override
    public void deleteList(List<DbPath> list) {
        DBPR.deleteInBatch(list);
    }

    @Override
    public long count() {
        return DBPR.count();
    }

    @Override
    public boolean isExist(String path) {
        return DBPR.exists(path);
    }

    @Override
    public DbPath duplicateOne(Path sp, Path rdp){
        DbPath dbp= DBPR.findOne(sp.toAbsolutePath().toString());
        dbp.setPath(rdp.toAbsolutePath().toString());
        dbp.setParent(rdp.getParent().toAbsolutePath().toString());
        dbp.setLink(FileService.generateLink());
        dbp.setTags("");
       return DBPR.save(dbp);
    }

    @Override
    public List<DbPath> duplicateList(List<Path> slp, List<Path> dp) {


        return null;
    }


    private void checkVault(String rootpath) {
        Path rp = Paths.get(rootpath);
        List<DbPath> list = new ArrayList();
        try {
            Files.walkFileTree(rp,new FileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    DbPath dbp = new DbPath();
                    dbp.setPath(dir.toAbsolutePath().toString());
                    dbp.setName(dir.getFileName().toString());
                    dbp.setIsDir((byte)1);
                    dbp.setCreateTime(SDF.format(attrs.creationTime().toMillis()));
                    dbp.setLastModTime(SDF.format(attrs.lastModifiedTime().toMillis()));
                    dbp.setLink(FileService.generateLink());
                    dbp.setOwner("System");
                    dbp.setType("Folder");
                    dbp.setSize(attrs.size());
                    dbp.setParent(dir.getParent().toAbsolutePath().toString());
                    dbp.setTags("");
                    list.add(dbp);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    DbPath dbp = new DbPath();
                    dbp.setPath(file.toAbsolutePath().toString());
                    dbp.setName(file.getFileName().toString());
                    dbp.setIsDir((byte)0);
                    dbp.setCreateTime(SDF.format(attrs.creationTime().toMillis()));
                    dbp.setLastModTime(SDF.format(attrs.lastModifiedTime().toMillis()));
                    dbp.setLink(FileService.generateLink());
                    dbp.setOwner("System");
                    dbp.setType(Files.probeContentType(file));
                    dbp.setSize(attrs.size());
                    dbp.setParent(file.getParent().toAbsolutePath().toString());
                    dbp.setTags("");
                    list.add(dbp);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    return FileVisitResult.SKIP_SIBLINGS;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        DBPR.save(list);
    }
}
