package out;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by zelenin on 05.09.16.
 */
public interface DbPathRepository extends JpaRepository<DbPath,String>,JpaSpecificationExecutor<DbPath> {

}
