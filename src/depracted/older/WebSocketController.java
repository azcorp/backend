package root.controller;


import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.*;
import root.repository.mongo.DbPathService;


import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.nio.file.FileSystems;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by zelenin on 26.09.16.
 */
@Controller
public class WebSocketController implements WebSocketHandler {
    private final DbPathService DBPSI;
    private final ObjectMapper mapper = new ObjectMapper();
    private static final Logger log = Logger.getLogger(WebSocketController.class);
    private static SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy hh:mm");
    private String FS = FileSystems.getDefault().getSeparator();
    private List<WebSocketSession> list = new LinkedList<>();
    @Resource(name = "rootpath") String rootPath;

    @Autowired
    public WebSocketController(DbPathService DBPSI) {
        this.DBPSI = DBPSI;
    }

    @PostConstruct
    void init(){
        log.info("Create "+WebSocketController.class);
    }


    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
        this.list.add(webSocketSession);
        System.out.println(this.list);
    }


    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
        System.out.println(webSocketMessage.getPayload());
        JsonNode jn = mapper.readTree(webSocketMessage.getPayload().toString());
        Map answer = new HashMap();
       switch (jn.get("channel").getTextValue()){
           case "PathModel":
               switch (jn.get("act").getTextValue()){
                   case "autocomplete":
                      answer.put("act","autocomplete");
                      answer.put("channel","PathModel");
                      answer.put("data",WsDbPathService.autocomplete(
                              jn.get("data").get("type").asText(),
                              jn.get("data").get("name").getTextValue(),
                              DBPSI));
                       webSocketSession.sendMessage(new TextMessage(mapper.writeValueAsString(answer)));
                       answer.clear();
                       break;
                   case "search":
                       answer.put("act","search");
                       answer.put("channel","PathModel");
                       answer.put("data",WsDbPathService.search(
                               jn.get("data").get("type").asText(),
                               jn.get("data").get("name").getTextValue(),
                               DBPSI));
                       webSocketSession.sendMessage(new TextMessage(mapper.writeValueAsString(answer)));
                       answer.clear();
                       break;
               }
               break;

           default:
               webSocketSession.sendMessage(new TextMessage("No DAO for"+jn.get(1).getTextValue()));
               break;
       }


    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {

    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {

    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }





}
