package out;

import java.nio.file.Path;
import java.util.List;

/**
 * Created by zelenin on 05.09.16.
 */
public interface DbPathServiceInterface {
    DbPath getByPath(String path);
    DbPath getByLink(String link);
    List<DbPath> getByParnet(String path);
    DbPath saveOrUpdateOne(DbPath dbp);
    List<DbPath> saveOrUpdateList(List<DbPath> list);
    void deleteOne(String path);
    void deleteList(List<DbPath> list);
    long count();
    DbPath duplicateOne(Path sp, Path rdp);
    List<DbPath> duplicateList(List<Path> slp,List<Path> dp);
    boolean isExist(String path);
}
