package root.service;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import root.model.DbPath;
import root.repository.mongo.DbPathService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by zelenin on 28.09.16.
 */
public class WsDbPathService {
  public static List<String> autocomplete(String type, String name, DbPathService dao){
    List<String> list =new ArrayList<>();
      dao.complexSearch(Query.query(
              Criteria.where("type")
              .regex(type,"i")
              .and("name")
              .regex("^"+name+".*$","i"))
              .with(new Sort(Sort.Direction.ASC,"name"))
              .limit(10))
              .forEach(dbPath -> list.add(dbPath.getName()));
   return list;
  }
  public static Set<DbPath> search(String type, String name, DbPathService dao){
      return  dao.complexSearch(Query.query(
              Criteria.where("type")
                      .regex(type,"i")
                      .and("name")
                      .regex(name,"i"))
      ).stream().collect(Collectors.toSet());
  }
}
