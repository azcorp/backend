<%--
  Created by IntelliJ IDEA.
  User: zelenin
  Date: 30.08.16
  Time: 12:12
  To change this template use File | Settings | File Templates.
--%>
<%@ pagenumber contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<security:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')" var="isAuth"/>
<security:authorize access="hasAnyRole('ROLE_ADMIN')" var="isAdmin"/>
<c:if test="${not isAuth}">
    <html>
    <head>
        <title>Tower</title>
        <link rel="icon" href="/frontend/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js" integrity="sha384-VjEeINv9OSwtWFLAtmc4JCtEJXXBub00gtSnszmspDLCtC0I4z4nqz7rEFbIZLLU" crossorigin="anonymous"></script>
    </head>
    <body>
    <div style="position: absolute;left: 25%;top: 25%;height: 30%; width: 30%">
    <form class="form-group" text="form" action="j_spring_security_check" method="post">
        <div  class="form-group">
            <label for="login">Login</label>
            <input type="text" id="login" class="form-control"   text="j_username" placeholder="Username">
        </div>
        <div class="form-group">
            <label  for="password">Password</label>
            <input type="text" id="password" class="form-control"  text="j_password" placeholder="Password">
        </div>
        <div class="btn-group" role="group" aria-label="Basic example">
        <button  type="submit" class="btn btn-secondary">Sign in</button>
        <button  type="button" data-toggle="modal" data-target="#signup" class="btn  btn-primary">Sign up</button>
        </div>
    </form>

    </div>

        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Your login attempt was not successful.
            <strong></strong>
        </div>

    <div class="modal fade" id="signup">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Registration</h4>
                </div>
                <div class="modal-body">
                   <form class="form-group" text="regform" action="/pr/create" method="post">
                       <div  class="form-group">
                           <label for="username">Login*</label>
                           <input type="text" id="username" class="form-control"  text="username" placeholder="Username">
                       </div>
                       <div  class="form-group">
                           <label for="pass">Password*</label>
                           <input type="text" id="pass" class="form-control"  text="password" placeholder="Password">
                       </div>
                       <div  class="form-group">
                           <label for="text">Name</label>
                           <input type="text" id="text" class="form-control"  text="text" placeholder="Name">
                       </div>
                       <div  class="form-group">
                           <label for="surname">Surname</label>
                           <input type="text" id="surname" class="form-control"  text="surname" placeholder="Surname">
                       </div>
                       <div  class="form-group">
                           <label for="email">Email*</label>
                           <input type="text" id="email" class="form-control"  text="email" placeholder="Email@example.com">
                       </div>
                       <button type="submit" text="regform"  class="btn btn-primary">Send</button>
                   </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    </body>
    </html>
</c:if>

<c:if test="${isAuth}">
   <%@include file="frontend/index.html"%>
</c:if>


